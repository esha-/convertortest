package ru.bmstu.tp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.bmstu.tp.ConvertorTestPage;

import java.io.File;
import java.lang.Math;

public class ConvertorTestTest {

    private static final Logger LOG = LoggerFactory.getLogger(ConvertorTestTest.class);
    private WebDriver webDriver;
    private ConvertorTestPage page;

    @Parameters({"browser", "url"})
    @BeforeClass
    public void SetupBrowser(String browser, String url) {
        LOG.debug("Using {} browser", browser);
        WebDriver driver;
        if (browser.equals("chrome")) {
            File file = new File("drivers/chromedriver");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
        driver.get(url);
        webDriver = driver;
        page = new ConvertorTestPage(driver);
    }

        
    // test currency locator  
    @Test
    public void CurrencyLocator () {
	  	page.fromTo("AUD", "EUR");
	    page.setInputValue("1");
	    assertEquals (page.getInputCurrency(), "австралийский доллар");
    	assertEquals (page.getOutputCurrency(), "евро");
    }
  
   //test click button "Reverse currency "
   @Test
  	public void TestReverseButton () {
	    page.fromTo("USD", "RUB");
	    page.setInputValue("1");
	    page.ClickReverse();
  		assertEquals (page.getInputCurrency(), "российский рубль");
  		assertEquals (page.getOutputCurrency(), "доллара сша");
  		page.ClickReverse();
  		assertEquals (page.getInputCurrency(), "доллар сша");
    	assertEquals (page.getOutputCurrency(), "российского рубля");
  	}
    
    //test changing currency locator
    @Test
    public void ChangeCurrencyLoc () {
    	page.setInputCurrency("USD");
    	page.setInputValue("1");
    	assertEquals(page.getInputCurrency(),"доллар сша");
    	page.setInputValue("2");
    	assertEquals(page.getInputCurrency(),"доллара сша");
    	page.setInputValue("10");
    	assertEquals(page.getInputCurrency(),"долларов сша");
    	
    }
    
  //test correct format value
    @Parameters("corrVal")
   	@Test
   	public void CorrectVal(String corrVal) {
    	page.setInputValue(corrVal);
   		String style = page.getInputValueColorStyle();
   		assertEquals(style, "color: rgb(0, 0, 0);");
   	}
    
    //test incorrect format value   
    //Why "100 000" ("3 400", "200 200 101" and other)  is correct output value, but this is incorrect input value ?
    // "10000" usd = "319 445" rub - true
    // BUT "319 445"rub = "10000" usd - incorrect 
     
    @Parameters("uncorrVal")
   	@Test
   	public void UncorrectVal(String uncorrVal) {
    	page.setInputValue(uncorrVal);
   		String style = page.getInputValueColorStyle();
   		assertEquals(style, "color: red;");
   	}
    
    //test 0 = 0 
    @Test
    public void NullIsNull() {
    	page.setInputValue("0");   	
    	assertEquals(page.getOutputValue(), "0");    	  	
    }    
    
    // test small value
    //test run	0.00001 usd = 0.0003 rub
    //test failure	0.000001 usd = 0 rub
    //test failure	0.0000001 usd = 0 rub 
    //BUT 0.00000001 usd = 3,1945×10-7 rub
    @Test
    public void SmallValue() {
    	page.fromTo("USD", "RUB");
    	page.setInputValue("0.00001");
    	assertTrue(Double.parseDouble(page.getOutputValue().replace( ',', '.' ) ) > 0.0 );    	  	
    }  
        
    //test converting from usd (Integer and Double) to ruble
    
    @Parameters("intVal")
    @Test
    public void IntUsdToRuble(Integer intVal) {
    	page.fromTo("USD", "RUB");
    	page.setInputValue(String.valueOf(intVal));
    	Double res= Double.parseDouble(page.getOutputValue().replace(',','.'));
    	Double calc= intVal*page.toUsdCourse;
    	assertEquals(res, calc);    	  	
    }  
    
    @Parameters("doubleVal")
    @Test
    public void DoubleUsdToRuble(Double doubleVal) {
    	page.fromTo("USD", "RUB");
    	page.setInputValue(String.valueOf(doubleVal));
    	Double res= Double.parseDouble(page.getOutputValue().replace(',','.'));
    	Double calc= doubleVal*page.toUsdCourse;
    	calc = Math.rint(10000.0 * calc) / 10000.0;
    	assertEquals(res, calc);    	  	
    }
      
          
    @AfterClass
    public void CloseDriver() {
        webDriver.quit();
    }

}
