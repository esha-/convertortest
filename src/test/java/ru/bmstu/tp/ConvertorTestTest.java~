package ru.bmstu.tp;

import org.joda.time.LocalTime;
import org.joda.time.Period;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: alex
 * Date: 25.10.13
 */
public class SunriseSunsetTest {

    private static final Logger LOG = LoggerFactory.getLogger(SunriseSunsetTest.class);
    private WebDriver webDriver;
    private SunriseSunsetPage page;

    @Parameters({"browser", "url"})
    @BeforeClass
    public void SetupBrowser(String browser, String url) {
        LOG.debug("Using {} browser", browser);
        WebDriver driver;
        if (browser.equals("chrome")) {
            File file = new File("drivers/chromedriver");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
        driver.get(url);
        webDriver = driver;
        page = new SunriseSunsetPage(driver);
    }

    @Test
    @Parameters({"pattern", "cityToSearch"})
    public void testCity(String pattern, String city) {
        page.inputInSearchbox(pattern + " " + city);
        assertEquals(page.getCurrentCity(), city);
    }

    @Test
    public void testSunriseBeforeSunset() {
        LocalTime sunrise = page.getSunriseTime();
        LocalTime sunset = page.getSunsetTime();
        assertTrue(sunset.compareTo(sunrise) > 0);
    }

    @Test
    public void testDayDuration() {
        LocalTime sunrise = page.getSunriseTime();
        LocalTime sunset = page.getSunsetTime();
        Period dayDuration = page.getDayDuration();
        assertEquals(sunrise.plus(dayDuration), sunset);
    }

    @Test
    public void testCitySuggests() {
        page.inputInCitybox("Vj"); //"Мо" в неправильной раскладке
        String suggests = page.getCitySuggests();
        LOG.debug("Suggests: {}", suggests);
        assertTrue(suggests.contains("Москва"));
        assertTrue(suggests.contains("Монтгомери"));
        assertFalse(suggests.contains("Харьков"));
    }

    @Test
    public void testDateChange() {
        page.setDayOfMonth(15);
        assertEquals(page.getCurrentDayOfMonth(), 15);
    }

    @AfterClass
    public void CloseDriver() {
        webDriver.quit();
    }

}
