package ru.bmstu.tp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ConvertorTestPage {

    private WebDriver webDriver;

    private final By inputValue = By.xpath("//*[@id=\"ival\"]");
    private final By outputValue = By.xpath("//*[@id=\"oval\"]");
    
    private final By inputCurrency = By.xpath("//*[@id=\"icode\"]");
    private final By outputCurrency = By.xpath("//*[@id=\"ocode\"]"); 
    
    private final By outRubCurrency = By.xpath("//ul[@id='select_second']//span[contains(@data-code, 'RUB')]");
    private final By inRubCurrency = By.xpath("//ul[@id='select_first']//span[contains(@data-code, 'RUB')]");
    
    private final By outUsdCurrency = By.xpath("//ul[@id='select_second']//span[contains(@data-code, 'USD')]");
    private final By inUsdCurrency = By.xpath("//ul[@id='select_first']//span[contains(@data-code, 'USD')]");
    
    private final By outEurCurrency =  By.xpath("//ul[@id='select_second']//span[contains(@data-code, 'EUR')]");
	private final By inEurCurrency = By.xpath("//ul[@id='select_first']//span[contains(@data-code, 'EUR')]");
	
	private final By outAudCurrency =  By.xpath("//ul[@id='select_second']//span[contains(@data-code, 'AUD')]");
	private final By inAudCurrency = By.xpath("//ul[@id='select_first']//span[contains(@data-code, 'AUD')]");
	
	private final By reverseLocator = By.xpath("//*[@id=\"change_conv\"]"); 
    
    public double toRubCourse;
    public double toUsdCourse;
    
    public ConvertorTestPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.getCourses();
    }
    //methods inputValue
    
    public void setInputValue(String inVal) {
    	WebElement element = webDriver.findElement(inputValue);
    	element.clear();
    	element.sendKeys(inVal);
    }
    
    public String getInputValueColorStyle() {
		WebElement element = webDriver.findElement(inputValue);
		return element.getAttribute("style");
	}
    
    //methods outputValue
    
    public String getOutputValue() {
    	String oval = webDriver.findElement(outputValue).getText();
    	return oval;
    }

    //methods inputCurrency
    
    public String getInputCurrency() {
    	String icur = webDriver.findElement(inputCurrency).getText();
    	return icur;    	  	
    }
    
    public void setInputCurrency(String Locator) {
		ClickElement(inputCurrency);
		if (Locator=="RUB") ClickElement(inRubCurrency);
		if (Locator=="USD") ClickElement(inUsdCurrency);
		if (Locator=="EUR") ClickElement(inEurCurrency);
		if (Locator=="AUD") ClickElement(inAudCurrency);
	}
    
   //methods outputCurrency 
      
    public String getOutputCurrency() {
    	String ocur = webDriver.findElement(outputCurrency).getText();
    	return ocur;    	  	
    }
       
    public void setOutputCurrency(String Locator) {
		ClickElement(outputCurrency);
		if (Locator=="RUB") ClickElement(outRubCurrency);
		if (Locator=="USD") ClickElement(outUsdCurrency);
		if (Locator=="EUR") ClickElement(outEurCurrency);
		if (Locator=="AUD") ClickElement(outAudCurrency);
	}
    //method set from Currency to Currency 
    
    public void fromTo (String inputCur, String outputCur) {
    	this.setInputCurrency(inputCur);
    	this.setOutputCurrency(outputCur);
    }
    
    private void getCourses() {
		WebElement elementRub = webDriver.findElement(inRubCurrency);
		toRubCourse = Double.parseDouble(elementRub.getAttribute("data-value"));
		WebElement elementUsd = webDriver.findElement(inUsdCurrency);
		toUsdCourse = Double.parseDouble(elementUsd.getAttribute("data-value"));
	}
    
    //methods reverseLocator
    
    public void ClickReverse() 	{
		ClickElement (reverseLocator);
	}

    private void ClickElement(By locator) {
		WebElement element = webDriver.findElement(locator);
		element.click();
	}
    
}
    
  